package com.five.controller;

import com.five.common.DataGridView;
import com.five.common.TreeNode;
import com.five.domain.SysPermission;
import com.five.service.PermissionService;
import com.five.vo.PermissionVo;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/admin/menu")
public class MenuController {
    @Autowired
    private PermissionService permissionService;

    //左侧导航栏的json菜单数据
    @RequestMapping("/loadIndexLeftMenuTreeJson")
    @PreAuthorize("hasPermission('/admin',null)")
    public List<TreeNode> loadIndexLeftMenuTreeJson(PermissionVo permissionVo){
        permissionVo.setType(1);
        List<TreeNode> nodes = permissionService.loadIndexLeftPermissionTreeJson(permissionVo);
        return nodes;
    }

    //加载菜单页面左边菜单树
    @RequestMapping("/loadMenuManagerLeftTreeJson")
    @PreAuthorize("hasPermission('/admin/menu/toMenuManager',null)")
    public DataGridView loadMenuManagerLeftTreeJson(PermissionVo permissionVo){
        permissionVo.setType(1);
        return permissionService.loadLeftManagerPermissionTreeJson(permissionVo);
    }

    //加载菜单页面下拉菜单树
    @RequestMapping("/loadMenuManagerDownTreeJson")
    @PreAuthorize("hasPermission('/admin/menu/addMenu','menu:add')"+"||hasPermission('/admin/menu/updateMenu','menu:update')")
    public DataGridView loadMenuManagerDownTreeJson(PermissionVo permissionVo){
        permissionVo.setType(1);
        permissionVo.setId(1);
        return permissionService.loadLeftManagerPermissionTreeJson(permissionVo);
    }

    //加载菜单列表
    @RequestMapping("/loadAllMenu")
    @PreAuthorize("hasPermission('/admin/menu/toMenuManager',null)")
    public DataGridView loadAllMenu(PermissionVo permissionVo){
        permissionVo.setType(1);
        Page<Object> page = PageHelper.startPage(permissionVo.getPage(),permissionVo.getLimit());
        List<SysPermission> data = permissionService.queryAllPermission(permissionVo);
        Iterator<SysPermission> menuIterator = data.iterator();
        while (menuIterator.hasNext()) {
            SysPermission menuTemp = menuIterator.next();
            if (menuTemp.getId()==1) {
                menuIterator.remove();
            }
        }
        return new DataGridView(page.getTotal(),data);
    }

    //添加菜单
    @RequestMapping("/addMenu")
    @PreAuthorize("hasPermission('/admin/menu/addMenu','menu:add')")
    public DataGridView addMenu(PermissionVo permissionVo){
        try {
            permissionVo.setType(1);
            permissionService.addPermission(permissionVo);
            return DataGridView.ADD_SUCCESS;
        }catch (Exception e){
            e.printStackTrace();
            return DataGridView.ADD_ERROR;
        }
    }

    //修改菜单
    @RequestMapping("/updateMenu")
    @PreAuthorize("hasPermission('/admin/menu/updateMenu','menu:update')")
    public DataGridView updateMenu(PermissionVo permissionVo){
        try {
            permissionService.updatePermission(permissionVo);
            return DataGridView.UPDATE_SUCCESS;
        }catch (Exception e){
            e.printStackTrace();
            return DataGridView.UPDATE_ERROR;
        }
    }

    //查询当前的ID的菜单有没有子菜单
    @RequestMapping("/checkMenuHasChildrenNode")
    @PreAuthorize("hasPermission('/admin/menu/deleteMenu','menu:delete')")
    public Map<String,Object> checkMenuHasChildrenNode(PermissionVo permissionVo){
        Map<String, Object> map=new HashMap<String, Object>();
        List<SysPermission> list = permissionService.queryAllPermission(permissionVo);
        if(list.size()>1) {
            map.put("value", true);
        }else {
            map.put("value", false);
        }
        return map;
    }

    //删除菜单
    @RequestMapping("/deleteMenu")
    @PreAuthorize("hasPermission('/admin/menu/deleteMenu','menu:delete')")
    public DataGridView deleteMenu(Integer id){
        try {
            permissionService.deletePermission(id);
            return DataGridView.DELETE_SUCCESS;
        }catch (Exception e){
            e.printStackTrace();
            return DataGridView.DELETE_ERROR;
        }
    }
}
