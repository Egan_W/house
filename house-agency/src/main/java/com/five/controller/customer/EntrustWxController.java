package com.five.controller.customer;


import com.five.common.DataGridView;
import com.five.domain.Entrust;
import com.five.service.EntrustService;
import com.five.service.HouseService;
import com.five.vo.EntrustVo;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/user/entrust")
public class EntrustWxController {
    @Autowired
    EntrustService entrustService;

    //发布委托
    @RequestMapping("/addEntrust")
    public DataGridView addEntrust(EntrustVo entrustVo){
        return entrustService.addEntrust(entrustVo);
    }

    //返回该用户的所有委托
    @RequestMapping("/queryEntrust")
    public DataGridView queryEntrust(EntrustVo entrustVo){
        List<Entrust> data = entrustService.queryEntrustByUser(entrustVo);
        return new DataGridView(200,"",data);
    }

    //取消委托
    @RequestMapping("/cancel")
    public DataGridView cancel(EntrustVo entrustVo){
        return entrustService.deleteEntrust(entrustVo);
    }
}
