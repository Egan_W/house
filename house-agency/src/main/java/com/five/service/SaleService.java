package com.five.service;

import com.five.common.DataGridView;
import com.five.domain.Sale;
import com.five.vo.SaleVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SaleService {

    //查询类别
    List<Sale> queryAllSale(SaleVo saleVo);

    //导航栏类别查询
    List<Sale> querySaleByUse(SaleVo saleVo);

    //添加类别
    DataGridView addSale(SaleVo saleVo);

    //修改类别信息
    DataGridView updateSale(SaleVo saleVo);

    //删除类别信息
    DataGridView deleteSale(SaleVo saleVo);
}
