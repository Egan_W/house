package com.five.service.impl;

import com.five.common.DataGridView;
import com.five.domain.City;
import com.five.domain.Decoration;
import com.five.mapper.DecorationMapper;
import com.five.service.DecorationService;
import com.five.vo.CityVo;
import com.five.vo.DecorationVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DecorationServiceImpl implements DecorationService {
    @Autowired
    private DecorationMapper decorationMapper;

    //查询装修风格信息
    @Override
    public List<Decoration> queryAllDecoration(DecorationVo decorationVo){
        List<Decoration> data = decorationMapper.queryAllDecoration(decorationVo);
        return data;
    }

    //导航栏装修风格查询
    public List<Decoration> queryDecorationByUse(DecorationVo decorationVo){
        List<Decoration> data = decorationMapper.queryDecorationByUse(decorationVo);
        return data;
    }

    //添加装修风格信息
    @Override
    public DataGridView addDecoration(DecorationVo decorationVo){
        DataGridView dataGridView = new DataGridView();
        if(!decorationMapper.queryAllDecoration(decorationVo).isEmpty()){
            dataGridView.setCode(500);
            dataGridView.setMsg("装修风格已存在！！");
        }else {
            decorationVo.setAvailable(1);
            decorationMapper.alter();
            int row =decorationMapper.addDecoration(decorationVo);
            if(row == 0){
                dataGridView.setCode(500);
                dataGridView.setMsg("添加失败，请重新添加！！");
            }else {
                dataGridView.setCode(200);
                dataGridView.setMsg("添加成功！！");
            }
        }
        return dataGridView;
    }

    //修改装修风格信息
    @Override
    public DataGridView updateDecoration(DecorationVo decorationVo){
        DataGridView dataGridView = new DataGridView();
        if(decorationMapper.queryDecorationByStyle(decorationVo.getStyle()) != null){
            dataGridView.setCode(500);
            dataGridView.setMsg("装修风格已存在！！");
        }else {
            int row = decorationMapper.updateDecoration(decorationVo);
            if(row == 1){
                dataGridView.setCode(200);
                dataGridView.setMsg("修改成功！！");
            }else {
                dataGridView.setCode(500);
                dataGridView.setMsg("修改失败！！");
            }
        }
        return dataGridView;
    }

    //删除(恢复)装修风格信息
    @Override
    public DataGridView deleteDecoration(DecorationVo decorationVo){
        DataGridView dataGridView = new DataGridView();
        int row = decorationMapper.deleteDecoration(decorationVo);
        if(row == 1){
            dataGridView.setMsg("设置成功！！");
            dataGridView.setCode(200);
        }else{
            dataGridView.setCode(500);
            dataGridView.setMsg("设置失败！！");
        }
        return dataGridView;
    }
}
