package com.five.service;

import com.five.common.DataGridView;
import com.five.domain.BaseEntity;
import com.five.domain.Order;
import com.five.vo.OrderVo;

import java.util.List;

public interface OrderService {

    //查询订单
    List<Order> queryAllOrder(OrderVo orderVo);

    //添加订单
    String addOrder(OrderVo orderVo) throws Exception;

    //查看订单
    DataGridView lookOrder(OrderVo orderVo);

    //取消订单
    void cancelOrder(OrderVo orderVo) throws Exception;

    //支付订单
    void payOrder(String id,Integer inputnum) throws Exception;

    //支出情况
    List<Order> queryAllOrderByBid(OrderVo orderVo);

    //收入情况
    List<Order> queryAllOrderBySaid(OrderVo orderVo);

    //统计销售额
    List<Double> loadOrderYear(Integer year);

    //统计城市员工年度销售额
    List<BaseEntity> LoadStaffCity(Integer city);

    //统计城市年度销售额
    List<Double> loadCityYear(Integer year, Integer city);

    //统计员工每月销售额
    List<Double> loadStaffYear(Integer year, String staff);

    //统计订单总数
    DataGridView countOrder(Order order);

    //统计新房销售额
    List<Double> loadNewOrder(Integer year);
}
