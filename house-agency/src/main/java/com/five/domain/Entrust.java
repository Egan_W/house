package com.five.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class Entrust {
    private String id;
    private String saId;
    private String name;
    private String phone;
    private int cId;
    private String cName;
    private String local;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date time;
    private Integer type;

    public Entrust() {
    }

    public Entrust(String id, String saId, String name, String phone, int cId, String cName, String local, Date time, Integer type) {
        this.id = id;
        this.saId = saId;
        this.name = name;
        this.phone = phone;
        this.cId = cId;
        this.cName = cName;
        this.local = local;
        this.time = time;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSaId() {
        return saId;
    }

    public void setSaId(String saId) {
        this.saId = saId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getcId() {
        return cId;
    }

    public void setcId(int cId) {
        this.cId = cId;
    }

    public String getcName() {
        return cName;
    }

    public void setcName(String cName) {
        this.cName = cName;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Entrust{" +
                "id='" + id + '\'' +
                ", saId=" + saId +
                ", name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", cId=" + cId +
                ", cName=" + cName +
                ", local='" + local + '\'' +
                ", time=" + time +
                ", type=" + type +
                '}';
    }
}
