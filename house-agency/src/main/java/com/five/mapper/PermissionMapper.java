package com.five.mapper;

import com.five.domain.SysPermission;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface PermissionMapper {
    //查询权限表
    List<SysPermission> queryAllPermission(SysPermission sysPermission);
    //修改时查询权限的子节点或父节点
    List<SysPermission> queryAllPermission2(SysPermission sysPermission);
    //添加菜单或权限
    int addPermission(SysPermission sysPermission);
    //修改菜单或权限
    int updatePermission(SysPermission sysPermission);
    //删除菜单或权限
    int deletePermission(Integer id);
    //通过pid删除权限与角色表的关联
    int deleteRolePermissionPid(@Param("pid") Integer id);
    //根据角色ID查询菜单
    List<SysPermission> queryPermissionByRoleId(@Param("available")Integer available, @Param("rid")Integer rid);
    //根据用户ID查询菜单
    List<SysPermission> queryPermissionByUserId(@Param("available")Integer available, @Param("uid")String uid,@Param("type")Integer type);
}
