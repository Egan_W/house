package com.five.mapper;

import com.five.domain.Sale;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SaleMapper {
    //查询类别信息
    List<Sale> queryAllSale(Sale sale);

    //导航栏查询
    List<Sale> querySaleByUse(Sale sale);

    //根据name查询
    Sale querySaleByName(@Param("name") String name);

    //添加类别
    void alter();
    int addSale(Sale sale);

    //修改类别信息
    int updateSale(Sale sale);

    //删除类别信息
    int deleteSale(Sale sale);
}
