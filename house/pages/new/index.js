import {request} from "../../request/index.js";
// pages/news/index.js
Page({
  data: {
    newObj: {},
  },
  New:{
    id:0
  },
  onShow: function () {
    let pages = getCurrentPages();
    let currentPage = pages[pages.length - 1];
    let options = currentPage.options;
    const {id}=options;
    this.getNewDetail(id);
  },
  // 获取房屋详情数据
  async getNewDetail(id) {
    const res = await request({ url: "/user/new/queryNewId", data:{id:id} });
    this.setData({
      newObj: res.data
    })
  },
})